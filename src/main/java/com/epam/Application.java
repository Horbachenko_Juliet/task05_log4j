package com.epam;


import org.apache.logging.log4j.*;

public class Application {
    public static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        logger.trace("This is a trace massage");
        logger.debug("This is a debug massage");
        logger.info("This is an info massage");
        logger.warn("This is a warn massage");
        logger.error("This is an error massage");
        logger.fatal("This is a fatal massage");
    }
}
